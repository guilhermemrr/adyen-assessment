## Adyen Assessment
This project was generated with [yo vanillajs generator](https://github.com/andyhass/generator-vanillajs)
version 0.1.13.

## Required before start
You should have `bower` and `npm` installed in your machine to run this project.

## Installing dependencies
Run `bower install` and `npm install` to install dependencies.
This project is using Bootstrap 3 and jQuery 1.9.0. For tests, it's using qUnit and Sinon.

## Running
Running `grunt server` will start the project at http://localhost:9000/

## Testing
Running `grunt test` will run the unit tests with qUnit at http://localhost:9000/

## Observations
The venues are ordered ascending by distance. So the user will see first the nearest places around.