module("VenuesResult Tests", {
  setup: function() {
    tableElement = $('<table class="hidden"> <tbody id="table-body"> </tbody> </table>');
  },
});

test('Constructor', function() {
  var venuesResult = new VenuesResult(tableElement);
  ok(venuesResult, 'VenuesResult object exists');
});

test('should return table element', function() {
  var venuesResult = new VenuesResult(tableElement);
  
  var element = venuesResult._getElement();
  equal(tableElement[0], element[0]);
});

test('should render list of venues ordered asc by distance', function(){
  var unorderedVenues = [
    {
      name: 'Seans',
      location: {
        distance: 100,
      }
    },
    {
      name: 'KFC',
      location: {
        distance: 50,
      }
    },
  ];

  var venuesResult = new VenuesResult(tableElement);
  venuesResult.setResult(unorderedVenues);

  var $el = venuesResult._getElement();
  var tableRows = $el.find('tr');
  
  equal(tableRows.length, 2);
  equal($(tableRows[0]).find('.distance').text(), 50);
  equal($(tableRows[1]).find('.distance').text(), 100);

});