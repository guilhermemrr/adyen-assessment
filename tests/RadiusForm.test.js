module("RadiusForm Tests", {
  setup: function() {
    formElement = $('<form><input id="radius" type="number" required><button type="submit"> Search </button></form>');
  },
});

test('Constructor', function() {
  var radiusForm = new RadiusForm(formElement);
  ok(radiusForm, 'RadiusForm object exists');
});

test('should trigger search event with radius value', function() {
  var radiusForm = new RadiusForm(formElement);
  var spy = sinon.spy();

  radiusForm.on('search', spy);

  formElement.find('input').val(500);
  formElement.trigger('submit');

  equal(spy.args[0][1], 500);
});

test('should not trigger search event when radius is empty', function() {
  var radiusForm = new RadiusForm(formElement);
  var spy = sinon.spy();

  radiusForm.on('search', spy);
  formElement.trigger('submit');

  equal(spy.called, false);
});

