module("Geolocation Tests", {
  setup: function() {
    geolocationElement = $('<div id="geolocation-alert"></div>');
  },
});

test('Constructor', function() {
  var geolocationElement = new Geolocation(geolocationElement);
  ok(geolocationElement, 'Geolocation object exists');
});

test('should return undefined user location object', function() {
  var geolocation = new Geolocation(geolocationElement);
  var location = geolocation.getLocation();

  equal(location.latitude, undefined);
  equal(location.longitude, undefined);
});