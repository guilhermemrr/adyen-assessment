module("FoursquareApi Tests", {
  setup: function() {
    radius = 500;
    foursquareApi = new FoursquareApi();
    requests = [];

    xhr = sinon.useFakeXMLHttpRequest();
    xhr.onCreate = function(req) {
      requests.push(req);
    }
  },
  teardown: function() {
    xhr.restore();
  },
});

test('Constructor', function() {
    ok(foursquareApi, 'FoursquareApi object exists');
});

test('should request with the correct latitude, longitude and radius', function() {
  var expectedUrl = 'https://api.foursquare.com/v2/venues/search?v=20170101&ll=53.4256576,-7.9223977&radius=500&client_id=I4JQKV3MZ5JAU4DKFBOD3WH531ZYHL340PEOUBKFZI4HH2CV&client_secret=0X4V5ULGHAOG3ISGTOEVDUFTTM2O3L0EOPRZX5IGT5AUMWAP&intent=browse';
  var location = {
    latitude: 53.4256576,
    longitude: -7.9223977
  };
  foursquareApi.getVenues(location, radius);
  equal(requests[0].url, expectedUrl);
});

test('should return a promise', function() {
  var location = {
    latitude: 53.4256576,
    longitude: -7.9223977
  };
  var promise = foursquareApi.getVenues(location, radius);

  ok(promise.then instanceof Function);
});

test('should not do the request if it has not all parameters', function() {
  var location = {
    latitude: 53.4256576,
    longitude: -7.9223977
  };

  foursquareApi.getVenues(location);
  equal(requests.length, 0);
});

test('should return promise even if has insufficient data', function() {
  var location = {
    latitude: 53.4256576,
    longitude: -7.9223977
  };

  var promise = foursquareApi.getVenues(location);
  ok(promise.then instanceof Function);
});