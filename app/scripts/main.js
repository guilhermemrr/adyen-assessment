(function($) {

  /* Create application modules */
  var foursquareApi = new FoursquareApi();

  var geolocation = new Geolocation( $('#geolocation-alert') );
  var radiusForm = new RadiusForm( $('#form-radius') );
  var venuesResult= new VenuesResult( $('#table-venues') );

  /* Request user location*/
  geolocation.requestUserLocation();

  /* Trigger search when the user submit a radius */
  radiusForm.on('search', function(event, radius){
    foursquareApi.getVenues(geolocation.getLocation(), radius).then(function(resp) {
      var venues = resp.response.venues;
      venuesResult.setResult(venues);
    });
  });

}(jQuery));