/*
  Module responsible for manage the submit requests maded by the user
*/

var RadiusForm = function (formElement) {

  /* Trigger search event with the radius inserted by the user */
  var _handleSubmit = function(event) {
    event.preventDefault();
    var radius = $(this.$el.find('input')).val();
    if(radius)
      this.$el.trigger('search', Number(radius));
  };

  this.$el = $(formElement);
  this.$el.on('submit', _handleSubmit.bind(this));

  return this.$el;
};