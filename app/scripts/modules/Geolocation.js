/*
  Module responsible for retrieve and store the user location 
*/

var Geolocation = function (geoElement) {

  var $el = $(geoElement);

  var latitude;
  var longitude;

  /* Store the user location */
  function _storePosition(position) {
    $el.addClass('hidden');
    latitude = position.coords.latitude;
    longitude = position.coords.longitude;
  }

  /* Show error to user */
  function _showLocationError(error) {
    $el.append('User denied the request for Geolocation.');
    $el.removeClass('hidden');
  }

  return {

    /* Request to browser ask permission for access to user location */
    requestUserLocation: function() {
      if (navigator.geolocation)
        navigator.geolocation.getCurrentPosition(_storePosition, _showLocationError);
    },

    /* Return user location */
    getLocation: function() {
      return { latitude: latitude, longitude, longitude };
    },
  }

};