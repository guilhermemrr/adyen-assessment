/*
  Module responsible for show the venues inside a table, sorted ascending by distance 
*/

var VenuesResult = function (tableElement) {

  var $el = $(tableElement)

  function _sortVenuesByDistance(venues) {
    return venues.sort(function(a,b) {
      return a.location.distance - b.location.distance;
    });
  };

  return {
    
    /* Render venues objects inside a table */ 
    setResult: function(venues) {
      venues = _sortVenuesByDistance(venues);
      $el.removeClass('hidden');
      $el.find('#table-body').empty();
      venues.forEach(function(venue){
        var address = venue.location.address || '';
        $el.find('#table-body').append('<tr> <td>' + venue.name + '</td> <td class="distance">'+ venue.location.distance +'</td>  <td>' + address + '</td>  </tr>')
      });
    },
    
    /* Return jquery element that holds the table structure */
    _getElement: function() {
      return $el;
    }
  };

};