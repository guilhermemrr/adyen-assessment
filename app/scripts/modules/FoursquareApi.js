/*
  Module responsible for consume the Foursquare Venues API 
*/

var FoursquareApi = function() {
    
    function createRequestUrl(lat, lng, radius){
      var clientId = 'I4JQKV3MZ5JAU4DKFBOD3WH531ZYHL340PEOUBKFZI4HH2CV';
      var clientSecret = '0X4V5ULGHAOG3ISGTOEVDUFTTM2O3L0EOPRZX5IGT5AUMWAP';
      var url = 'https://api.foursquare.com/v2/venues/search?v=20170101&ll=' + lat + ',' + lng + '&radius=' + radius + '&client_id=' + clientId + '&client_secret=' + clientSecret + '&intent=browse';  
      return url;
    }

    return {
      
        /* Request venues around given an location object with properties latitude and logitude, and a radius */
        getVenues: function(location, radius) {
          var invalidParameters = isNaN(location.latitude) || isNaN(location.longitude) || isNaN(radius);
          if(invalidParameters)
            return Promise.reject('Invalid parameters');

          var url = createRequestUrl(location.latitude, location.longitude, radius);
          return new Promise(function(resolve, reject) {
            $.ajax({
              url: url,
              success: function(result) {
                resolve(result);
              },
            });
          });
        },

    };

};